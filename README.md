# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Validate-lib 是基于 github 上的 [validate4j](https://github.com/CFutureTeam/validate4j)；因为原来的项目无法满足自己需要，故在此创建一个新的项目，希望对此感兴趣的一起参与 :)
项目介绍可以直接参照原来项目的README，其是一个用于Java环境的，易于使用，可扩展的,可定义校验顺序的数据校验库。
它内建15种常用验证规则，只需要简单配置即可使用；它也提供一个扩展接口，继承并实现Tester的方法即可自定义校验器。

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact